#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import urllib

# Routines for getting cover images from some source or other

baseurl   = "http://www.amazon.com/s?k=%s"
imgs      = re.compile("<img src=\"(https://m.media-amazon.com/[^\"]+jpg)\"")
covername = "cover.jpg"

def getCover(artist, title, cdir = ""):
	artist = artist.replace(" ", "+")
	artist = artist.replace("_", "+")
	artist = artist.replace("'", "")
	title = title.replace("'", "")
	title = title.replace(" ", "+")
	title = title.replace("_", "+")
	
	u = baseurl % (artist + "+" + title)
	s = urllib.urlopen(u).readlines()
	imgurl = ""
	for l in s:
		m = imgs.search(l)
		if m != None:
			imgurl = m.group(1)
			break
	if imgurl == "":
		print("***** Unable to find image URL *****")
		print ("at %s" % (u))
		return ""
	print("Found image: " + m.group(1))
	return makeCover(m.group(1), cdir)


def makeCover(u, cdir = ""):
	try:
		s = urllib.urlopen(u).read()
	except:
		print("Unable to get " + u)
		return
	try:
		if cdir == "":
			cfile = covername
		else:
			cfile = cdir + "/" + covername
		cvr = open(cfile, 'w')
		cvr.write(s)
		cvr.close()
	except:
		print("***** Unable to make cover.jpg *****")
		return ""
	return cfile


def makeThumbnail(img):
	from PIL import Image
	im = Image.open(img)
	#print im.size
	im.thumbnail((100,100), Image.ANTIALIAS)
	im.save(img)


def coverExists(cdir):
	return os.path.exists(cdir + "/" + covername)


usage = '''getcover.py artist title
Attempts to retrieve a cover image for `title' by `artist'
'''

if __name__ == "__main__":
	import sys
	try:
		artist = sys.argv[1]
		title = sys.argv[2]
	except:
		print(usage)
		sys.exit(1)
	cvr = getCover(artist, title)
	if not cvr:
		sys.exit(1)
	makeThumbnail(cvr)

