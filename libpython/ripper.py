#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys
import os
import shutil
import CDDB
import DiscID
import getcover

# CDDB.py still expects freedb, which is dead
cddb_server = "http://gnudb.gnudb.org/~cddb/cddb.cgi"

multid  = re.compile("_\W*_+")
befdot  = re.compile("_(?=\.)")
quote   = re.compile(r"['\"]")
enddot  = re.compile("[._]$")
muldot  = re.compile("\.\.+")
dotdash = re.compile("\.(?=_)")
theaetc = re.compile("^(?:the|a|an)_")
muldash = re.compile("__+")

def asciize(s, chardeny = "^-a-zA-Z0-9_.", no_the = False):
	keepers = re.compile("[" + chardeny + "]")
	s = quote.sub("", s)
	s = keepers.sub("_", s.lower())
	s = multid.sub("_", s)
	s = befdot.sub("", s)
	s = muldot.sub(".", s)
	s = enddot.sub("", s)
	s = dotdash.sub("", s)
	s = muldash.sub("_", s)
	if no_the:
		s = theaetc.sub("", s)
	return s


def dasciize(s, no_the = False):
	'''Same but no dots'''
	return asciize(s, chardeny = "^-a-zA-Z0-9_", no_the = no_the)


def execcmd(cmd, debug = False):
	if debug:
		sys.stderr.write(cmd + "\n")
		return
	success = os.system(cmd)
	if success:
		sys.stdout.write("Command `%s' failed! Aborting!\n" % (cmd))
		sys.exit(success)


# Calculate the disc id
# Unsued, and seems to be broken with gnudb, anyway
def calcid(id):
	numtracks = id[1]
	trax = hex(numtracks)[2:]
	if len(trax) < 2:
		trax = "0" + trax
	time = id[-1] - (id[2] / 75)
	time = hex(time)[2:]
	while len(time) < 4:
		time = "0" + time
	
	times = id[2:-1]
	chksum = 0
	for t in times:
		s = str(t / 75)
		c = 0
		for n in s:
			c += int(n)
		chksum += c
	chksum = hex(chksum % 255)[2:]
	if len(chksum) < 2:
		chksum = "0" + chksum
	return chksum + time + trax


def int2track(i):
	j = str(i)
	if len(j) < 2:
		j = "0" + j
	return "track" + j + ".cdda.wav"


# Implements a grip-like syntax for specifying filenames
def makeTuneName(s, tun, extn, startnum):
	s = s.replace("%n", tun.normednum(startnum))
	s = s.replace('%t', tun.title)
	s = s.replace('%T', tun.cd.title)
	s = s.replace('%a', tun.artist)
	s = s.replace('%A', tun.cd.artist)
	s = s.replace('%y', tun.cd.year)
	s = s.replace('%c', tun.cd.catgry)
	s = s.replace('%g', tun.cd.genre)
	s = s.replace('%x', extn)
	return s


# Implements a grip-like syntax for specifying dirnames
def makeDirName(s, cd, extn):
	s = s.replace('%T', dasciize(cd.title))
	s = s.replace('%A', dasciize(cd.artist, no_the = True))
	s = s.replace('%y', dasciize(cd.year))
	s = s.replace('%c', dasciize(cd.catgry))
	s = s.replace('%g', dasciize(cd.genre))
	s = s.replace('%x', extn)
	return s


# Wraps the call to cdparanoia
def cdparanoia(ropts, numtrax):
	if not ropts.debug:
		cmd = "cdparanoia -d " + ropts.cddev + " -B " + ropts.cdpopts
		execcmd(cmd)
		return

	# If debugging, then we fake it and just create numtrax files
	# sanity check
	cmd = "cdparanoia -d " + ropts.cddev + " -B " + ropts.cdpopts
	print cmd
	if numtrax > 100:
		print "Won't create more than 100 files!"
		sys.exit(1)
	for i in range(1, numtrax + 1):
		cmd = "touch " + int2track(i)
		execcmd(cmd)


class ripformat:
	def __init__(self, cmd, extn, opts, fname):
		self.cmd   = cmd
		self.extn  = extn
		self.opts  = opts
		self.fname = fname


class ripperopts:
	def __init__(self):
		self.chardeny = "^-a-zA-Z0-9_."
		#FIXME Sensible default
		self.cddev    = "/dev/sr0"
		self.cdpopts  = ""
		self.debug    = False
		self.discnum  = 0
		self.docover  = True
		self.editor   = "kwrite"
		self.filelocn = ""
		self.formats  = {}
		self.keeptemp = False
		self.ripfmt   = "--output-wav"
		self.starttrk = 1
		self.tagfile  = ""
		self.tworips  = True
		self.verbose  = False
		self.writinto = False


class cdinfo:
	def __init__(self):
		self.discid  = ""
		self.artist  = ""
		self.genre   = ""
		self.year    = ""
		self.catgry  = ""
		self.title   = ""
		self.graphic = ""
		self.tunes   = []
		self.numtrax = 0

	def addtune(self, tune):
		self.tunes.append(tune)

	def __repr__(self):
		retval = ["DISCID: " + self.discid, "DISC ARTIST: " + self.artist, "DISC TITLE: " + self.title, "DISC YEAR: " + self.year, "DISC CATEGORY: " + self.catgry, "DISC GENRE: " + self.genre, "GRAPHIC: " + self.graphic]
		for f in self.tunes:
			retval.append(repr(f))
		return "\n".join(retval)


class tune:
	def __init__(self, cd, num, title, artist = ""):
		remfeat = re.compile("\s*\(?feat(?:\.|uring) .*\)?$")
		title = remfeat.sub("", title)
		self.cd       = cd
		self.num      = num
		self.title    = title
		self.artist   = artist
		self.cdpname  = int2track(self.num)
		self.convname = ""
		self.ripped   = False

	def normednum(self, startnum):
		n = str(self.num + startnum - 1)
		if len(n) < 2:
			n = "0" + n
		return n

	def __repr__(self):
		rval = "TITLE" + self.normednum(1) + ": " + self.title
		if self.artist:
			rval += "\nTITLE" + self.normednum() + "ARTIST: " + self.artist
		return rval


def showChoices(discid, info, ropts):
	i = 1
	for inf in info:
		try:
			cd = getCDInfo(discid, inf, ropts)
		except:
			print "Failed to get CD info for %s" % inf
			continue
		print "Number", i
		print "--------"
		print repr(cd)
		print
		i += 1
	val = raw_input("Select record... ")
	val = int(val)
	if val <= 0 or val > len(info):
		return 0
	return val


def getCDInfo(discid, discinfo, ropts):
	cd = cdinfo()
	cd.numtrax = discid[1]
	cd.catgry = discinfo["category"]
	cd.discid = discinfo["disc_id"]
	
	(status, info) = CDDB.read(cd.catgry, cd.discid, cddb_server)
	
	if status != 210:
		print "Error getting info for disc " + cd.catgry + "/" + cd.discid
		raise LookupError, "FDRogs"

	(cd.artist, cd.title) = info['DTITLE'].split('/', 1)
	cd.artist = cd.artist.strip()
	cd.title = cd.title.strip()
	if info.has_key('DYEAR'):
		cd.year = info['DYEAR'].strip()
	if info.has_key('DGENRE'):
		cd.genre = info['DGENRE']

	#FIXME Other info on this tune? Esp matters for multi-artist
	for i in range(0, cd.numtrax):
		#cd.addtune(tune(cd, ropts.starttrk + i, info['TTITLE' + str(i)].strip()))
		cd.addtune(tune(cd, i + 1, info['TTITLE' + str(i)].strip()))
	return cd


def makeflactag(key, val, val2 = ""):
	if not val:
		val = val2
	if not val:
		return ""
	val = val.replace("\"", "\\\"")
	return " -T " + key + "=\"" + val + "\""


def wav2flac(curtune, ropts):
	format = ropts.formats['flac']
	newtname = makeTuneName(format.fname, curtune, format.extn, ropts.starttrk)
	newtname = asciize(newtname, ropts.chardeny)
	curtune.convname = newtname
	cmd = format.cmd + " " + format.opts + " -o " + newtname
	cmd += makeflactag("TITLE", curtune.title)
	cmd += makeflactag("TRACKNUMBER", str(curtune.num))
	cmd += makeflactag("GENRE", curtune.cd.genre, curtune.cd.catgry)
	cmd += makeflactag("ARTIST", curtune.cd.artist)
	cmd += makeflactag("ALBUM", curtune.cd.title)
	cmd += makeflactag("DATE", curtune.cd.year)
	cmd += makeflactag("PERFORMER", curtune.artist)
	cmd += " " + curtune.cdpname
	if ropts.debug:
		print cmd
		os.system("touch " + newtname)
		return
	if ropts.verbose:
		print "About to execute: " + cmd
	execcmd(cmd)


def movetune(cd, ropts, extn):
	multi = cd.artist.find(',')
	if multi == -1:
		return movetunes(cd, ropts, extn)

	artists = cd.artist.split(',')
	art = artists[0].strip()
	tmpcd = cd
	tmpcd.artist = art
	realdir = makeDirName(ropts.filelocn, cd, extn)
	# Need to remove the makeDirName bit from that routine and
	# do it here so we know it. Of course, we'll also have to add it above.
	success = movetunes(tmpcd, ropts, extn)
	if not success:
		return False

	artists = artists[1:]
	for art in artists:
		art = art.strip()
		tmpcd.artist = art
		linkdir = makeDirName(ropts.filelocn, cd, extn)
		if os.path.exists(linkdir):
			print "Link directory:", linkdir, "already exists."
			continue
		try:
			os.symlink(realdir, linkdir)
			print "Linked", linkdir, "to", realdir
		except:
			print "Unable to link", linkdir, "to", realdir

	return True


def movetunes(cd, ropts, extn):
	newdir = makeDirName(ropts.filelocn, cd, extn)
	if ropts.debug:
		newdir = "/tmp/" + newdir	
	elif not os.path.exists(newdir):
		if not ropts.writinto:
			os.makedirs(newdir)
	elif not os.path.isdir(newdir):
		print newdir, "exists but is not a directory."
		print "Aborting copy."
		return False
	elif not ropts.writinto:
		# move old directory
		dircp = newdir[0:-1]
		while os.path.exists(dircp):
			dircp += ".old"
		os.rename(newdir, dircp)
		print newdir, "renamed to", dircp
		os.makedirs(newdir)

	for curtune in cd.tunes:
		tname = curtune.convname
		newtname = newdir + tname
		if ropts.debug:
			print "move:", tname, "to", newtname
		else:
			shutil.move(tname, newtname)
	if not ropts.debug and os.path.exists('cover.jpg'):
		shutil.move('cover.jpg', newdir + 'cover.jpg')

	print "Copied new files to", newdir

	return True


def readCDInfo(fname):
	try:
		infile = open(fname)
		lines = infile.readlines()
	except:
		print "Unable to read from file `" + fname + "'!"
		sys.exit(1)

	infile.close()

	direg = re.compile("^DISCID:\s*(.*(..))")
	dareg = re.compile("^DISC ARTIST:\s*(.*)")
	dtreg = re.compile("^DISC TITLE:\s*(.*)")
	dyreg = re.compile("^DISC YEAR:\s*(.*)")
	dcreg = re.compile("^DISC CATEGORY:\s*(.*)$")
	dgreg = re.compile("^DISC GENRE:\s*(.*)$")
	ttreg = re.compile("^TITLE(\d\d):\s*(.*)")
	tareg = re.compile("^TITLE(\d\d)ARTIST:\s*(.*)")
	grreg = re.compile("^GRAPHIC:\s*(.*)")
	
	thiscd = cdinfo()

	tunelist = {}
	for line in lines:
		m = direg.match(line)
		if m != None:
			thiscd.discid = m.group(1)
			thiscd.numtrax = int(m.group(2), 16)
			continue
		m = dareg.match(line)
		if m != None:
			thiscd.artist = m.group(1)
			continue
		m = dtreg.match(line)
		if m != None:
			thiscd.title = m.group(1)
			continue
		m = dyreg.match(line)
		if m != None:
			thiscd.year = m.group(1)
			continue
		m = dcreg.match(line)
		if m != None:
			thiscd.catgry = m.group(1)
			continue
		m = dgreg.match(line)
		if m != None:
			thiscd.genre = m.group(1)
			continue
		m = grreg.match(line)
		if m != None:
			thiscd.graphic = m.group(1)
			continue
		m = ttreg.match(line)
		if m != None:
			num = m.group(1)
			name = m.group(2)
			tunelist[num] = tune(thiscd, int(num), name)
			continue
		m = tareg.match(line)
		if m != None:
			num = m.group(1)
			name = m.group(2)
			if not tunelist.has_key(num):
				print "Error: Got artist info for " + num + " without tune info!"
				continue
			tunelist[num].artist = name
			continue
		sys.stderr.write("Unrecognized Line: " + line)

	for t in tunelist:
		thiscd.addtune(tunelist[t])
	return thiscd


def findImage(cd):
	return getcover.getCover(cd.artist, cd.title)

