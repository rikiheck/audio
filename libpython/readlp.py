#!/usr/bin/env python2

import re
import sys
from urllib import urlopen

class lp:
	def __init__(self):
		self.artist = ""
		self.genre  = ""
		self.cddb   = ""
		self.cover  = ""
		self.title  = ""
		self.year   = ""
		self.filelist = []
	def addfile(self, file):
		self.filelist.append(file)
	def __repr__(self):
		retval = [self.artist, self.title, self.genre, self.cddb]
		for f in self.filelist:
			retval.append(repr(f))
		return "\n".join(retval)

class tune:
	def __init__(self, name = "", start = "", end = ""):
		self.name  = name
		self.start = start
		self.end   = end
	def __repr__(self):
		return "(" + self.name + "," + self.start + "," + self.end + ")"

class wavfile:
	def __init__(self, name = ""):
		self.name  = name.strip()
		self.tunes = []
	def addtune(self, tune):
		self.tunes.append(tune)
	def __repr__(self):
		retval = ["name: " + self.name]
		for tune in self.tunes:
			retval.append("\t" + repr(tune))
		return "\n".join(retval)

def nextTune(flist, cf, ct):
	ct += 1
	if ct < len(flist[cf].tunes):
		return (cf, ct)
	ct = 0
	cf += 1
	if cf < len(flist):
		return (cf, ct)
	return (None, None)

def readlp(fname, ignore):
	try:
		infile = open(fname)
		lines = infile.readlines()
	except:
		print( "Unable to read from file `" + fname + "'!" )
		sys.exit(1)
	
	infile.close()
	
	areg = re.compile("^Artist:\s*(.*)")
	treg = re.compile("^Title:\s*(.*?)")
	creg = re.compile("^CDDB:\s*(.*)")
	vreg = re.compile("^Cover:\s*(.*)")
	freg = re.compile("^File:\s*(?:file:\/\/)?(\S.*?)\s*$")
	greg = re.compile("^Genre:\s*(.*)$")
	yreg = re.compile("^Year:\s*(\d\d\d\d)\s*$")
	ereg = re.compile("^EndFile")
	sreg = re.compile("(\d+:\d\d(?:\.\d\d?)?)(?:\s+(\d+:\d\d)(?:\.\d\d?)?)?\s*(.*)")
	comment = re.compile("^\s*#")
	blank   = re.compile("^\s*$")
	
	curFile = False
	
	thislp = lp()
	
	for line in lines:
		if comment.match(line) or blank.match(line):
			continue
		if not curFile:
			m = areg.match(line)
			if m != None:
				thislp.artist = m.group(1).strip()
				continue
			m = treg.match(line)
			if m != None:
				thislp.title = m.group(1).strip()
				continue
			m = creg.match(line)
			if m != None:
				thislp.cddb = m.group(1).strip()
				continue
			m = vreg.match(line)
			if m != None:
				thislp.cover = m.group(1).strip()
				continue
			m = yreg.match(line)
			if m != None:
				thislp.year = m.group(1).strip()
				continue
			m = greg.match(line)
			if m != None:
				thislp.genre = m.group(1).strip()
				continue
			m = freg.match(line)
			if m != None:
				curFile = wavfile(m.group(1))
				continue
		else:	
			# We're reading file info
			m = ereg.match(line)
			if m != None:
				if len(curFile.tunes) == 0:
					sys.stderr.write("No tune info found for file `" + curFile.name + "'")
				else:
					thislp.addfile(curFile)
				curFile = False
				continue
			m = sreg.match(line)
			if m != None:
				tstart = m.group(1)
				tend = ""
				if m.group(2):
					tend = m.group(2)
				titl = m.group(3).strip()
				curFile.addtune(tune(titl, tstart, tend))
				continue
		sys.stderr.write("Unrecognized Line: " + line)
		sys.exit(1)
	
	if not thislp.filelist:
		sys.stderr.write("No file info found in file `" + fname + "'")
	
	curTune = 0
	curFile = 0
	
	if thislp.cddb:
		cddb = []
		try:
			cddb = urlopen(thislp.cddb).readlines()
		except:
			sys.stderr.write("Unable to open url: ", thislp.cddb)
	
		for line in cddb:
			line = line.strip()
			dtitl = re.compile("^DTITLE=([^/]*)/(.*)")
			tuner = re.compile("^TTITLE\d+=(.*)")
			genre = re.compile("^DGENRE=(\w+)")
			dyear = re.compile("^DYEAR=(\d+)")
			m = dtitl.match(line)
			if m != None:
				if not thislp.artist:
					thislp.artist = m.group(1).strip()
				if not thislp.title:
					thislp.title = m.group(2).strip()
				continue
			m = tuner.match(line)
			if m != None and curFile != None:
				if not ignore and curTune == None:
					print("Too many tunes in cddb file!\n")
					sys.exit(1)
				if not thislp.filelist[curFile].tunes[curTune].name:
					thislp.filelist[curFile].tunes[curTune].name = m.group(1).strip()
				(curFile, curTune) = nextTune(thislp.filelist, curFile, curTune)
				# print curTune
				continue
			m = genre.match(line)
			if m != None:
				if not thislp.genre:
					thislp.genre = m.group(1).strip()
			m = dyear.match(line)
			if m != None:
				if not thislp.year:
					thislp.year = m.group(1).strip()
		# Did we see all the tunes?
		if not ignore and curTune != None:
			print("Too few tunes in cddb file!\n")
			sys.exit(1)

	return thislp

