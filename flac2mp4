#!/usr/bin/perl
use strict;
use warnings;
use File::Copy;
use File::Path;
use Getopt::Std;
use Cwd;
use Audio::FLAC::Header;

# ffmpeg -i 01-anyhow.flac -c:a aac -b:a 256k 01-anyhow.m4a
# mp4tags -A "Let Me Get By" -a "Tedeschi Trucks Band" -g Blues -s "Anyhow" -y 2014 01-anyhow.m4a 

#die("Figure out how to skip multiple copies of an album in a single directory!");

sub escape
{
  my $in = shift;
  return "" unless $in;
  $in =~ s/"//g;
  return $in;
}

# param is flac filename
# returns ($artist, $title, $album, $date, $genre)
sub getflacinfo
{
  my $fil = shift;
  return ("", "", "", "") unless (-r $fil);
  #print $fil;
  my $f = Audio::FLAC::Header->new($fil);
  my $t = $f->tags();
  my $artist = escape($t->{ARTIST});
  my $title  = escape($t->{TITLE});
  my $album  = escape($t->{ALBUM});
  my $date   = $t->{DATE};
  my $genre  = escape($t->{GENRE});
  return ($artist, $title, $album, $date, $genre);
}

###############

my $mp4dir;
my $mp4options = "-c:a aac -b:a 256k -vcodec copy";

my $usage = <<EOT;
flac2mp4 [-cdv] [-o OUTDIR] <directory>
We will convert all flacs in <directory> to m4a files.
If not given, we work on the current directory.
Options:
-c: Overwrite existing m4a files
-d: Debug
-o: Ouput directory (default is input directory)
-v: Verbose output
EOT

my %options;
getopts(":dho:xv", \%options);

if (defined($options{h})) { 
  print $usage; 
  exit 0; 
}
our $clobber = defined($options{c});
our $debug   = defined($options{d});
our $outdir  = defined($options{o}) ? $options{o} : ".";
our $verbose = defined($options{v});

my $curdir = getcwd;
my $flacdir = shift;
if ($flacdir) {
  chdir $flacdir or die("Can't change to $flacdir!")
}

$outdir =~ s{/$}{};
if (!$debug && ! -d $outdir) {
	File::Path::make_path $outdir;
}

my @flacs = <*.flac>;
if (!@flacs) {
	print "No flacs found!\n";
	exit 0;
}

my $oldlength = 0;
my $didone = 0;
foreach my $flac (@flacs) {
	my $mp4 = $flac;
	$mp4 =~ s/\.flac$/.m4a/;
	# probably should clean this too
	$mp4 =~ s{/$}{};
	$mp4 = "$outdir/$mp4";
	if (-f $mp4 and ! $clobber) {
		my $flacmod = (stat($flac))[9];
		my $mp4mod  = (stat($mp4))[9];
		if ($flacmod <= $mp4mod) {
			print "$mp4 already exists. Use -c to force over-write.\n" if $verbose;
			next;
		}
		print "\nRegenerating due to more recent modtime...";
	}

	# get tag info
	my ($artist, $title, $album, $date, $genre) = getflacinfo($flac);

	print "\nConverting $flac";
	my $cmd = "ffmpeg -y -i $flac $mp4options $mp4";
	if ($debug) { print "\n$cmd\n"; } 
	else { 
		system("$cmd >/dev/null 2>&1");
	}
	die("$cmd\n$!\n") if $?;
	$didone = 1;
}
print "\nDone\n" if $didone and !$debug;

chdir $curdir;

