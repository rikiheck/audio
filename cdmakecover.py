#!/usr/bin/python

import sys
import os
import re
import getcover

sp = re.compile("/multi/[^/]+/([^/]+)/\d\d\d\d[a-z]?-([^/]+)/?")

def processDirectory(d):
	d = os.path.abspath(d)
	if getcover.coverExists(d):
		return True
	m = sp.match(d)
	if m == None:
		return False
	a = m.group(1)
	t = m.group(2)
	getcover.getCover(a, t, d)
	return True

s = os.getcwd()
u = ""
try:
	u = sys.argv[1]
except:
	pass

if u != "":
	cvr = getcover.makeCover(u, s)
	getcover.makeThumbnail(cvr)
	sys.exit(0)

if processDirectory(s):
	sys.exit(0)

dirs = os.listdir(s)
for d in dirs:
	if not os.path.isdir(d):
		print "Couldn't match " + d
		continue
	processDirectory(d)
