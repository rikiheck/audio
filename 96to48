#!/bin/bash

function printUsage {
	me=$0;
	me=${me##*/};
	echo $me;
	echo "Converts flacs from one sample rate and bit depth to another.
Options:
  -1: Convert to 44.1 kHz
  -4: Convert to 48 kHz
  -8: Convert to 88.2kHz
  -9: Convert to 96 kHz
  -C: Convert to CD format (44.1k/16)
  -b: Bit depth to convert to.
  -d: Don't do anything, just say what we'd do.
  -D: Write into existing directory.
  -g: Do not guard against clipping.
  -r: Sample rate to convert to.
  -h: Print this message.

Either a sample rate or bitdepth must be given for conversion.
";
}

function do96to48 {
	FIL="$1"; NEWDIR="$2";
	JUSTFIL=${FIL%.flac};
	if [ "$FIL" = "$JUSTFIL" ]; then
		echo "$FIL does not appear to be a FLAC!";
		exit 2;
	fi

	FIL48="$NEWDIR/$FIL";
	if [ -n "$SRATE" ]; then
		$DEBUG sox -S "$FIL" "$FIL48" -G $BITRATE rate -v $SRATE;
	elif [ -n "$BITRATE" ]; then
		$DEBUG sox -S "$FIL" "$FIL48" -G $BITRATE;
	else
		echo "No conversion requested!";
		echo "Should have been caught earlier!";
		exit 11;
	fi
}

BITDEPTH="";
BITRATE="";
DEBUG="";
DIREXIST="";
GAIN="-G";
SRATE="";

while getopts ":1489b:CdDgr:h" options $ARGS; do
  case $options in
    1)  SRATE="44.1k";;
    4)  SRATE="48k";;
    8)  SRATE="88.2k";;
    9)  SRATE="96k";;
    b)  BITDEPTH="$OPTARG";;
    C)  SRATE="44.1k"; BITDEPTH="16";;
    d)  DEBUG="echo";;
    D)  DIREXIST="OK";;
    g)  GAIN="";;
    r)  SRATE="rate -v $OPTARG";;
    h)  printUsage; exit 0;;
    \?) echo "Option $options not found.";
        printUsage; 
        exit 10;;
  esac
done

if [ -n "$BITDEPTH" ]; then 
	BITRATE="-b $BITDEPTH";
fi

if [ -z "$SRATE" -a -z "$BITRATE" ]; then
	printUsage;
	exit 1;
fi

shift $(($OPTIND - 1));

IFS="
";

dir="$1";

if [ -z "$dir" ]; then dir="."; fi

while [ -n "$dir" ]; do

	if ! pushd $dir >/dev/null; then 
		echo "Unable to change to $dir!"; 
		exit 1; 
	fi

	TESTFILE=$(ls -1 *.flac | head -n1);

	if [ -z "$BITDEPTH" ]; then
		FBDEPTH="$(soxi $TESTFILE | grep "^Precision" | perl -pe 's/.*: //; s/-.*//')";
	else
		FBDEPTH="$BITDEPTH";
	fi

	if [ -z "$SRATE" ]; then
		FSRATE="$(soxi $TESTFILE | grep "^Sample Rate" | perl -pe 's/.*: //; s/0+$//')";
	else
		FSRATE="$(echo $SRATE | perl -pe 's/[0k]+$//;s/\.\d+$//')";
	fi

	curdir=`pwd`;
	curdir=`basename "$curdir"`;
	if echo $curdir | grep -q -P -- '-\d\d\d?\.\d\d$'; then
		curdir=${curdir%-*};
	fi
	newdir=$curdir-$FSRATE.$FBDEPTH;

	if [ -z "$DIREXIST" -a -d "$newdir" ]; then 
		echo "$newdir already exists!";
		exit 2;
	fi

	$DEBUG mkdir $newdir;

	if ! ls *.flac 2>&1 >/dev/null; then
		echo "No flacs found!";
		exit 3;
	fi

	for i in `ls -1 *.flac`; do
		do96to48 "$i" "$newdir";
	done

	if [ -z "$DEBUG" ]; then
		if [ -f cover.jpg ]; then 
			cp cover.jpg $newdir;
		else 
			echo "No cover found!";
		fi
	fi

	popd >/dev/null;
	shift;
	dir="$1";

done
